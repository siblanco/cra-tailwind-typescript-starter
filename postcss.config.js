const tailwindcss = require('tailwindcss');
const purgecss = require('@fullhuman/postcss-purgecss');
const autoprefixer = require('autoprefixer');

module.exports = {
	plugins: [
		tailwindcss('./tailwind.config.js'),
		autoprefixer,
		process.env.NODE_ENV === 'production' &&
			purgecss({
				content: ['./src/**/*.tsx'],
				defaultExtractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
			}),
	],
};
