import React from 'react';
import loadable from '@loadable/component';
import Loader from '../components/common/Loader';

const LazyPage = (page: string) =>
	loadable(() => import(`../views/${page}`), {
		fallback: <Loader />,
	});

const LazyApp = (app: string) =>
	loadable(() => import(`../${app}`), {
		fallback: <Loader />,
	});

export { LazyPage, LazyApp };
