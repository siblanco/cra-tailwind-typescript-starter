import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import './tailwind.dist.css';

ReactDOM.render(<App />, document.getElementById('root'));
