import React from 'react';
import { LazyPage } from './utils/LazyComponent';

const Home = LazyPage('Home');

const App = () => {
	return <Home />;
};

export default App;
