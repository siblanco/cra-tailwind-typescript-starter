module.exports = {
	purge: false,
	theme: {
		fontFamily: {
			body: ['Roboto', 'sans-serif'],
		},
	},
};
